IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Titles')
CREATE TABLE [dbo].[CLONE_UK_Titles](
	[TitleID] [int]  NOT NULL,
	[Title] [nvarchar](30) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)


