IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Employees')
CREATE TABLE [dbo].[CLONE_UK_Employees](
	[EmployeeID] [int] NOT NULL,
	[LastName] [nvarchar](20) NOT NULL,
	[FirstName] [nvarchar](10) NOT NULL,
	[TitleID] [int] NULL,
	[TitleOfCourtesy] [nvarchar](25) NULL,
	[BirthDate] [date] NULL,
	[HireDate] [date] NULL,
	[Address] [nvarchar](60) NULL,
	[CityID] [int] NULL,
	[RegionID] [int] NULL,
	[PostalCode] [nvarchar](10) NULL,
	[HomePhone] [nvarchar](24) NULL,
	[Extension] [nvarchar](4) NULL,
	[Photo] [image] NULL,
	[Notes] [ntext] NULL,
	[ReportsTo] [int] NULL,
	[PhotoPath] [nvarchar](255) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)



