IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Countries')
CREATE TABLE [dbo].[CLONE_UK_Countries](
	[CountryID] [int] NOT NULL,
	[CountryName] [nvarchar](20) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)



