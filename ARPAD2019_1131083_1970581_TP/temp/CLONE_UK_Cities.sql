IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Cities')
CREATE TABLE [dbo].[CLONE_UK_Cities](
	[CityID] [int] NOT NULL,
	[CityName] [nvarchar](20) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)



