IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Regions')
CREATE TABLE [dbo].[CLONE_UK_Regions](
	[RegionID] [int] NOT NULL,
	[RegionDescription] [nchar](50) NOT NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)



