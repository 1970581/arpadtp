IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_OrderDetails')
CREATE TABLE [dbo].[CLONE_UK_OrderDetails](
	[OrderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[Discount] [float] NOT NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)



