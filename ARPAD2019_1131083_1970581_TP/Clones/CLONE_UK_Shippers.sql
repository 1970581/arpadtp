IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Shippers')
CREATE TABLE [dbo].[CLONE_UK_Shippers](
	[ShipperID] [int]  NOT NULL,
	[CompanyName] [nvarchar](40) NOT NULL,
	[Phone] [nvarchar](24) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)



