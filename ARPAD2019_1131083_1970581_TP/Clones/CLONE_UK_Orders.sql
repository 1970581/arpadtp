IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Orders')
CREATE TABLE [dbo].[CLONE_UK_Orders](
	[OrderID] [int] NOT NULL,
	[CustomerID] [int] NULL,
	[EmployeeID] [int] NULL,
	[OrderDate] [date] NULL,
	[RequiredDate] [date] NULL,
	[ShippedDate] [date] NULL,
	[ShipperID] [int] NULL,
	[Freight] [money] NULL,
	[ShipToID] [int] NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)



