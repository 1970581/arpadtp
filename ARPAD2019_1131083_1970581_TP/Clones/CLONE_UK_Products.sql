IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Products')
CREATE TABLE [dbo].[CLONE_UK_Products](
	[ProductID] [int]  NOT NULL,
	[ProductName] [nvarchar](40) NULL,
	[SupplierID] [int] NULL,
	[CategoryID] [int] NULL,
	[QuantityPerUnitID] [int] NULL,
	[UnitPrice] [money] NULL,
	[UnitsInStock] [smallint] NULL,
	[UnitsOnOrder] [smallint] NULL,
	[ReorderLevel] [smallint] NULL,
	[Discontinued] [bit] NOT NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)




