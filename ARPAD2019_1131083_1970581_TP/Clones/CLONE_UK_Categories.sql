IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'CLONE_UK_Categories')
CREATE TABLE [dbo].[CLONE_UK_Categories](
	[CategoryID] [int]  NOT NULL,
	[CategoryName] [nvarchar](15) NOT NULL,
	[Description] [ntext] NULL,
	[Picture] [image] NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)



