IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimSupplier')
BEGIN
CREATE TABLE [dbo].[DimSupplier](
	[SupplierKey] [int] IDENTITY(1,1) NOT NULL,
	[SupplierID] [int]  NULL,
	[CompanyName] [nvarchar](40)  NULL,
	[ContactName] [nvarchar](30)  NULL,
	[Title] [nvarchar](30)  NULL,
	[Address] [nvarchar](60)  NULL,
	[CityName] [nvarchar](20)  NULL,
	[RegionDescription] [nvarchar](50)  NULL,
	[PostalCode] [nvarchar](10)  NULL,
	[CountryName] [nvarchar](40)  NULL,
	[Phone] [nvarchar](24)  NULL,
	[Fax] [nvarchar](24)  NULL,
	[EffectiveDate] [date] NULL,
	[ExpiredDate] [date] NULL,
	[isCurrent] [nvarchar](3) NULL,
 CONSTRAINT [PK_DimSupplier] PRIMARY KEY CLUSTERED 
(
	[SupplierKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-SupplierKey] ON [dbo].[DimSupplier]
(
	[SupplierKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END

