IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimShipTo')
BEGIN

CREATE TABLE [dbo].[DimShipTo](
	[ShipToKey] [int] IDENTITY(1,1) NOT NULL,
	[ShipToID] [int] NULL,
	--[ShipToName] [nvarchar](30) NOT NULL,
	[ShipToName] [nvarchar](60)  NULL,
	--[ShipToAddress] [nvarchar](30) NOT NULL,
	[ShipToAddress] [nvarchar](60)  NULL,
	[ShipToCityName] [nvarchar](20)  NULL,
	[ShipToRegionDescription] [nvarchar](50)  NULL,
	[ShipToPostalCode] [nvarchar](10)  NULL,
	[ShipToCountryName] [nvarchar](20)  NULL
 CONSTRAINT [PK_DimShipTo] PRIMARY KEY CLUSTERED 
(
	[ShipToKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-ShipToKey] ON [dbo].[DimShipTo]
(
	[ShipToKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END
