IF NOT EXISTS (SELECT name FROM sys.tables where name = 'FactCurrency')
BEGIN
CREATE TABLE [dbo].[FactCurrency](
	[SourceCurrencyKey] [int] NOT NULL,
	[DestinationCurrencyKey] [int] NOT NULL,
	[DateKey] [int] NOT NULL,
	[ExchangeRate] [float] NOT NULL,
 CONSTRAINT [PK_FactCurrency] PRIMARY KEY CLUSTERED 
(
	[SourceCurrencyKey] ASC,
	[DestinationCurrencyKey] ASC,
	[DateKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END


