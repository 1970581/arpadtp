IF NOT EXISTS (SELECT name FROM sys.tables where name = 'FactOrder')
BEGIN

CREATE TABLE [dbo].[FactOrder](
	[ProductKey] [int] NOT NULL,
	[OrderDateKey] [int] NOT NULL,
	[EmployeesKey] [int] NOT NULL,
	[CustomerKey] [int] NOT NULL,
	[SupplierKey] [int] NOT NULL,
	[ShipperKey] [int] NOT NULL,
	[ShipToKey] [int] NOT NULL,
--	[CurrencyKey] [int] NOT NULL,
	[OrderStoreKey] [int] NOT NULL,
	[OrderID][int] NOT NULL,
	[RequiredDate][int]NOT NULL,
	[ShippedDate][int]NOT NULL,
--	[Freight][money]NOT NULL,
--	[FreightUSD][money]NOT NULL,
	[Quantity][smallint]NOT NULL,
	[Discount][float]NOT NULL,

    [CurrencyKey] [int] NOT NULL,           --Aparece em cima
	[UnitPrice][money]NOT NULL,				--Preco unitario
	[Freight][money]NOT NULL,
	[TotalWithoutDiscount][money]NOT NULL,	--Preco unitario x numero unidades
	[TotalWithDiscount][money]NOT NULL,		--Preco unitario x numero unidades x (1 - Desconto)
	[DiscountValue][money]NOT NULL,			--(Preco unitario x numero unidades) - (Preco unitario x numero unidades x (1 - Desconto))
	[TotalWithFreightNoDiscount][money]NOT NULL, -- Preco unitario x numero unidades + Freight
	[Total][money]NOT NULL,					-- Preco unitario x numero unidades x (1 - Desconto) + Freight

	[USD_CurrencyKey] [int] NOT NULL,
	[USD_UnitPrice][money]NOT NULL,				--Preco unitario
	[USD_Freight][money]NOT NULL,
	[USD_TotalWithoutDiscount][money]NOT NULL,	--Preco unitario x numero unidades
	[USD_TotalWithDiscount][money]NOT NULL,		--Preco unitario x numero unidades x (1 - Desconto)
	[USD_DiscountValue][money]NOT NULL,			--(Preco unitario x numero unidades) - (Preco unitario x numero unidades x (1 - Desconto))
	[USD_TotalWithFreightNoDiscount][money]NOT NULL, -- Preco unitario x numero unidades + Freight
	[USD_Total][money]NOT NULL,					-- Preco unitario x numero unidades x (1 - Desconto) + Freight

	[GBP_CurrencyKey] [int] NOT NULL,
	[GBP_UnitPrice][money]NOT NULL,				--Preco unitario
	[GBP_Freight][money]NOT NULL,
	[GBP_TotalWithoutDiscount][money]NOT NULL,	--Preco unitario x numero unidades
	[GBP_TotalWithDiscount][money]NOT NULL,		--Preco unitario x numero unidades x (1 - Desconto)
	[GBP_DiscountValue][money]NOT NULL,			--(Preco unitario x numero unidades) - (Preco unitario x numero unidades x (1 - Desconto))
	[GBP_TotalWithFreightNoDiscount][money]NOT NULL, -- Preco unitario x numero unidades + Freight
	[GBP_Total][money]NOT NULL,					-- Preco unitario x numero unidades x (1 - Desconto) + Freight


--	[UnitPriceUSD][money]NOT NULL,
--	[TotalWithDiscount][money]NOT NULL,
--	[TotalWithDiscountUSD][money]NOT NULL,
--	[TotalWithoutDiscount][money]NOT NULL,
--	[TotalWithoutDiscountUSD][money]NOT NULL,
--	[TotalWithFreight][money]NOT NULL,
--	[TotalWithFreightUSD][money]NOT NULL,
--	[Total][money]NOT NULL,
--	[TotalUSD][money]NOT NULL,

 CONSTRAINT [PK_FactOrder] PRIMARY KEY CLUSTERED 
(
	[ProductKey] ASC,
	[OrderDateKey] ASC,
	[OrderStoreKey] ASC,
--	[ShipToKey] ASC,
	[CustomerKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END

