IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimProduct')
BEGIN
CREATE TABLE [dbo].[DimProduct](
	[ProductKey] [int]IDENTITY(1,1) NOT NULL,
--	[SupplierKey] [int]  NULL,
	[ProductID] [int]  NULL,
	[ProductName] [nvarchar](40)  NULL,
	[CategoryName] [nvarchar](15)  NULL,
	[CategoryDescription] [nvarchar](255)  NULL,
	[UnitPrice] [money] NULL,
	[UnitPriceUS] [money] NULL,
	[ProductQuantityPerUnit] [nvarchar](30)  NULL,
	[Discontinued] [char](5) NULL,
	[EffectiveDate] [date] NULL,
	[ExpiredDate] [date] NULL,
	[isCurrent] [nvarchar](3)  NULL,
 CONSTRAINT [PK_DimProduct] PRIMARY KEY CLUSTERED 
(
	[ProductKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-ProductKey] ON [dbo].[DimProduct]
(
	[ProductKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END

