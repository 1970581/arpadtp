IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimCustomer')
BEGIN

CREATE TABLE [dbo].[DimCustomer](
	[CustomerKey] [int] IDENTITY(1,1) NOT NULL,
	--[SourceDelegationKey] [int]  NOT NULL,
	[CustomerID] [int] NOT NULL,
	--[CustomerID_UK] [int]  NULL,
	--[CustomerID_US] [char](10)  NULL,
	[CompanyName] [nvarchar](40) NULL,
	[ContactName] [nvarchar](30) NULL,
	[Title] [nvarchar](30) NULL,
	[Address] [nvarchar](60) NULL,
	[CityName] [nvarchar](20) NULL,
	[RegionDescription] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](10) NULL,
	[CountryName] [nvarchar](40) NULL,
	[Phone] [nvarchar](24) NULL,
	[Fax] [nvarchar](24) NULL,
	[EffectiveDate] [date] NULL,
	[ExpiredDate] [date] NULL,
	[IsCurrent] [nvarchar](3) NULL,
 CONSTRAINT [PK_DimCustomer] PRIMARY KEY CLUSTERED 
(
	[CustomerKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-CustomerKey] ON [dbo].[DimCustomer]
		(
			[CustomerKey] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


END

