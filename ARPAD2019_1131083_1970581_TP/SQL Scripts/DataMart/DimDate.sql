IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimDate')
Begin
	CREATE TABLE [dbo].[DimDate](
		[DateKey] [int] IDENTITY(1,1) NOT NULL,
		[FullDate] [datetime] NOT NULL,
		[Year] [int] NOT NULL,
		[Month] [tinyint] NOT NULL,
		[Semester] [tinyint] NOT NULL,
		[Quadrimestre] [tinyint] NOT NULL,
		[Trimester] [tinyint] NOT NULL,		
		[MonthName] [nvarchar](10) NOT NULL,
		[Week] [tinyint] NOT NULL,
		[Week_of_month] [tinyint] NOT NULL,
		[DayOfWeek] [nvarchar](10) NOT NULL,

		[DayNumberOfYear] [int] NOT NULL,
		[DayNumberOfMonth] [tinyint] NOT NULL,
		[DayNumberOfWeek] [tinyint] NOT NULL,		
		
		[Weekend] [nvarchar](3) NOT NULL,
		[Season] [nvarchar](10) NOT NULL,
		[yyyymmdd] [int]  NOT NULL,
		[yyyy-mm-dd] [nvarchar](10)  NOT NULL,
		
	
	 CONSTRAINT [PK_DimDate] PRIMARY KEY CLUSTERED 
	(
		[DateKey] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY]

	CREATE NONCLUSTERED INDEX [NonClusteredIndex-DateKey] ON [dbo].[DimDate]
(
	[DateKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
