IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DimEmployee')
BEGIN
CREATE TABLE [dbo].[DimEmployee](
	[EmployeeKey] [int] IDENTITY(1,1) NOT NULL,
	[SourceDelegationKey][int] NULL,
	[EmployeeID] [int] NULL,
	[LastName] [nvarchar](20) NULL,
	[FirstName] [nvarchar](10) NULL,
	[Title] [nvarchar](30) NULL,
	[Gender] [nvarchar](10) NULL,
	[BirthDate] [int] NULL,
	[HireDate] [int] NULL,
	[Address] [nvarchar](60) NULL,
	[CityName] [nvarchar](20) NULL,
	[RegionDescription] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](10) NULL,
	[HomePhone] [nvarchar](24) NULL,
	[ReportsTo] [nvarchar](60)  NULL,
	[EffectiveDate] [date] NULL,
	[ExpiredDate] [date] NULL,
	[isCurrent] [nvarchar](3) NULL,
 CONSTRAINT [PK_DimEmployee] PRIMARY KEY CLUSTERED 
(
	[EmployeeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-EmployeeKey] ON [dbo].[DimEmployee]
(
	[EmployeeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END

