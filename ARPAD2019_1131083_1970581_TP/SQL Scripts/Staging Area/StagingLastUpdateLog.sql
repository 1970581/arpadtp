if not exists (Select name from sys.tables where name ='StageLastUpdateLog')
Begin
CREATE TABLE [dbo].[StageLastUpdateLog](
	[Description] [nvarchar](20) NULL,
	[TitleLastUpdate] [date] NULL
) ON [PRIMARY]

INSERT [dbo].[StageLastUpdateLog] ([Description], [TitleLastUpdate]) VALUES (N'lastUpdate', CAST(N'1900-01-01' AS Date))
END