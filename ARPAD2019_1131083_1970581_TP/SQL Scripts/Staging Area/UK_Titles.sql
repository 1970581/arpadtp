IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'UK_Titles')
CREATE TABLE [dbo].[UK_Titles](
	[TitleID] [int]  NOT NULL,
	[Title] [nvarchar](30) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)
ELSE
	TRUNCATE TABLE UK_Titles

