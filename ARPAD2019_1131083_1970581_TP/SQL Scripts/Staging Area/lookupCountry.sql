﻿IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'lookupCountry')
BEGIN
CREATE TABLE [dbo].[lookupCountry](
	[countryCode] [char](3) NOT NULL,	
	[countryName] [nvarchar](60) NOT NULL,
	[countryIndicative] [nvarchar](24) NOT NULL,
	[currencyCode] [char](3) NOT NULL,
	[currencyName] [nvarchar](20) NULL	
	)

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'AFG', N'Afghanistan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ALB', N'Albania', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'DZA', N'Algeria', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ASM', N'American Samoa', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'AND', N'Andorra', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'AGO', N'Angola', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'AIA', N'Anguilla', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ATA', N'Antarctica', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ATG', N'Antigua and Barbuda', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ARG', N'Argentina', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ARM', N'Armenia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ABW', N'Aruba', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'AUS', N'Australia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'AUT', N'Austria', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'AZE', N'Azerbaijan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BHS', N'Bahamas (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BHR', N'Bahrain', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BGD', N'Bangladesh', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BRB', N'Barbados', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BLR', N'Belarus', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BEL', N'Belgium', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BLZ', N'Belize', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BEN', N'Benin', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BMU', N'Bermuda', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BTN', N'Bhutan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BOL', N'Bolivia (Plurinational State of)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BES', N'Bonaire, Sint Eustatius and Saba', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BIH', N'Bosnia and Herzegovina', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BWA', N'Botswana', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BVT', N'Bouvet Island', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BRA', N'Brazil', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'IOT', N'British Indian Ocean Territory (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BRN', N'Brunei Darussalam', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BGR', N'Bulgaria', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BFA', N'Burkina Faso', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BDI', N'Burundi', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CPV', N'Cabo Verde', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'KHM', N'Cambodia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CMR', N'Cameroon', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CAN', N'Canada', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CYM', N'Cayman Islands (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CAF', N'Central African Republic (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TCD', N'Chad', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CHL', N'Chile', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CHN', N'China', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CXR', N'Christmas Island', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CCK', N'Cocos (Keeling) Islands (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'COL', N'Colombia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'COM', N'Comoros (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'COD', N'Congo (the Democratic Republic of the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'COG', N'Congo (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'COK', N'Cook Islands (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CRI', N'Costa Rica', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'HRV', N'Croatia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CUB', N'Cuba', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CUW', N'Curaτao', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CYP', N'Cyprus', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CZE', N'Czech Republic', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CIV', N'C⌠te d''Ivoire', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'DNK', N'Denmark', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'DJI', N'Djibouti', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'DMA', N'Dominica', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'DOM', N'Dominican Republic (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ECU', N'Ecuador', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'EGY', N'Egypt', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SLV', N'El Salvador', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GNQ', N'Equatorial Guinea', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ERI', N'Eritrea', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'EST', N'Estonia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SWZ', N'Eswatini', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SWZ', N'Swaziland', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ETH', N'Ethiopia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'FLK', N'Falkland Islands (the) [Malvinas]', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'FRO', N'Faroe Islands (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'FJI', N'Fiji', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'FIN', N'Finland', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'FRA', N'France', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GUF', N'French Guiana', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PYF', N'French Polynesia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ATF', N'French Southern Territories (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GAB', N'Gabon', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GMB', N'Gambia, The', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GEO', N'Georgia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'DEU', N'Germany', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GHA', N'Ghana', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GIB', N'Gibraltar', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GRC', N'Greece', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GRL', N'Greenland', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GRD', N'Grenada', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GLP', N'Guadeloupe', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GUM', N'Guam', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GTM', N'Guatemala', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GGY', N'Guernsey', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GIN', N'Guinea', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GNB', N'Guinea-Bissau', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GUY', N'Guyana', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'HTI', N'Haiti', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'HMD', N'Heard Island and McDonald Islands', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'VAT', N'Holy See (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'HND', N'Honduras', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'HKG', N'Hong Kong', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'HUN', N'Hungary', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ISL', N'Iceland', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'IND', N'India', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'IDN', N'Indonesia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'IRN', N'Iran (Islamic Republic of)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'IRQ', N'Iraq', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'IRL', N'Ireland', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'IMN', N'Isle of Man', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ISR', N'Israel', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ITA', N'Italy', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'JAM', N'Jamaica', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'JPN', N'Japan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'JEY', N'Jersey', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'JOR', N'Jordan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'KAZ', N'Kazakhstan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'KEN', N'Kenya', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'KIR', N'Kiribati', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PRK', N'Korea (the Democratic People''s Republic of)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'KOR', N'Korea, South', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'KWT', N'Kuwait', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'KGZ', N'Kyrgyzstan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LAO', N'Lao People''s Democratic Republic (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LVA', N'Latvia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LBN', N'Lebanon', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LSO', N'Lesotho', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LBR', N'Liberia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LBY', N'Libya', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LIE', N'Liechtenstein', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LTU', N'Lithuania', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LUX', N'Luxembourg', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MAC', N'Macao', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MDG', N'Madagascar', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MWI', N'Malawi', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MYS', N'Malaysia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MDV', N'Maldives', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MLI', N'Mali', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MLT', N'Malta', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MHL', N'Marshall Islands (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MTQ', N'Martinique', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MRT', N'Mauritania', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MUS', N'Mauritius', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MYT', N'Mayotte', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MEX', N'Mexico', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'FSM', N'Micronesia (Federated States of)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MDA', N'Moldova (the Republic of)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MCO', N'Monaco', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MNG', N'Mongolia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MNE', N'Montenegro', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MSR', N'Montserrat', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MAR', N'Morocco', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MOZ', N'Mozambique', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MMR', N'Myanmar', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NAM', N'Namibia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NRU', N'Nauru', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NPL', N'Nepal', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NLD', N'Netherlands', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NCL', N'New Caledonia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NZL', N'New Zealand', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NIC', N'Nicaragua', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NER', N'Niger (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NGA', N'Nigeria', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NIU', N'Niue', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NFK', N'Norfolk Island', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MNP', N'Northern Mariana Islands (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'NOR', N'Norway', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'OMN', N'Oman', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PAK', N'Pakistan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PLW', N'Palau', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PSE', N'Palestine, State of', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PAN', N'Panama', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PNG', N'Papua New Guinea', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PRY', N'Paraguay', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PER', N'Peru', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PHL', N'Philippines', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PCN', N'Pitcairn', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'POL', N'Poland', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PRT', N'Portugal', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'PRI', N'Puerto Rico', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'QAT', N'Qatar', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MKD', N'Republic of North Macedonia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ROU', N'Romania', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'RUS', N'Russian Federation (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'RWA', N'Rwanda', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'REU', N'RΘunion', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'BLM', N'Saint BarthΘlemy', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'RUM', N'Réunion', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SHN', N'Saint Helena, Ascension and Tristan da Cunha', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'KNA', N'Saint Kitts and Nevis', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LCA', N'Saint Lucia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'MAF', N'Saint Martin (French part)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SPM', N'Saint Pierre and Miquelon', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'VCT', N'Saint Vincent and the Grenadines', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'WSM', N'Samoa', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SMR', N'San Marino', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'STP', N'Sao Tome and Principe', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SAU', N'Saudi Arabia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SEN', N'Senegal', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SRB', N'Serbia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SYC', N'Seychelles', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SLE', N'Sierra Leone', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SGP', N'Singapore', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SXM', N'Sint Maarten (Dutch part)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SVK', N'Slovakia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SVN', N'Slovenia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SLB', N'Solomon Islands', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SOM', N'Somalia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ZAF', N'South Africa', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SGS', N'South Georgia and the South Sandwich Islands', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SSD', N'South Sudan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ESP', N'Spain', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'LKA', N'Sri Lanka', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SDN', N'Sudan (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SUR', N'Suriname', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SJM', N'Svalbard and Jan Mayen', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SWE', N'Sweden', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'CHE', N'Switzerland', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'SYR', N'Syrian Arab Republic', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TWN', N'Taiwan (Province of China)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TJK', N'Tajikistan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TZA', N'Tanzania, United Republic of', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'THA', N'Thailand', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TLS', N'Timor-Leste', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TGO', N'Togo', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TKL', N'Tokelau', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TON', N'Tonga', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TTO', N'Trinidad and Tobago', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TUN', N'Tunisia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TUR', N'Turkey', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TKM', N'Turkmenistan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TCA', N'Turks and Caicos Islands (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'TUV', N'Tuvalu', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'UGA', N'Uganda', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'UKR', N'Ukraine', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ARE', N'United Arab Emirates (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'GBR', N'UK', N'(+351)', N'GBP', N'Pound sterling')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'UMI', N'United States Minor Outlying Islands (the)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'USA', N'USA', N'(+351)', N'USD', N'United States dollar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'URY', N'Uruguay', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'UZB', N'Uzbekistan', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'VUT', N'Vanuatu', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'VEN', N'Venezuela', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'VNM', N'Viet Nam', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'VGB', N'Virgin Islands (British)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'VIR', N'Virgin Islands (U.S.)', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'WLF', N'Wallis and Futuna', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ESH', N'Western Sahara', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'YEM', N'Yemen', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ZMB', N'Zambia', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ZWE', N'Zimbabwe', N'(+351)', N'USA', N'Dolar')

INSERT [dbo].[lookupCountry] ([countryCode], [countryName], [countryIndicative], [currencyCode], [currencyName]) VALUES (N'ALA', N'Åland Islands', N'(+351)', N'USA', N'Dolar')
END
