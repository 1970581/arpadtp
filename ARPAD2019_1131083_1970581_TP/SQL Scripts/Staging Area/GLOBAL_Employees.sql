IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'GLOBAL_Employees')
Begin
CREATE TABLE [dbo].[GLOBAL_Employees](
	[EmployeeID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID_UK] [int] NULL,
	[EmployeeID_US] [int] NULL,
	[Delegation] [nvarchar](4) NULL,
	[LastName] [nvarchar](20)  NULL,
	[FirstName] [nvarchar](10)  NULL,
	[TitleID_UK] [int] NULL,
	[Title] [nvarchar](35) NULL,
	[FullName] [nvarchar](55) NULL,
	[TitleOfCourtesy] [nchar](4) NULL,
	[Gender] [nchar](10) NULL,
	[BirthDate] [date] NULL,
	[HireDate] [date] NULL,
	[Address] [nvarchar](60) NULL,
	[CityID_UK] [int] NULL,
	[City] [nvarchar](20)  NULL,
	[RegionID] [int] NULL,
	[Region] [nchar](50)  NULL,
	[PostalCode] [nvarchar](10) NULL,
	[HomePhone] [nvarchar](24) NULL,
	[Extension] [nvarchar](4) NULL,
	[Photo] [image] NULL,
	[Notes] [ntext] NULL,
	[ReportsTo] [int] NULL,
	[PhotoPath] [nvarchar](255) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
 CONSTRAINT [PK_GLOBAL_Employees] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-EmployeeID] ON [dbo].[GLOBAL_Employees]
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END


