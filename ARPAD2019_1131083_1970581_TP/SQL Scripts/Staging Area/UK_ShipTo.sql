IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'UK_ShipTo')
CREATE TABLE [dbo].[UK_ShipTo](
	[ShipToID] [int]  NOT NULL,
	[ShipToName] [nvarchar](30) NULL,
	[ShipToAddress] [nvarchar](30) NULL,
	[ShipCityID] [int] NULL,
	[ShipRegionID] [int] NULL,
	[ShipPostalCode] [nvarchar](10) NULL,
	[ShipCountryID] [int] NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)
ELSE
	TRUNCATE TABLE UK_ShipTo


