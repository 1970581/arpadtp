IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'UK_Shippers')
CREATE TABLE [dbo].[UK_Shippers](
	[ShipperID] [int]  NOT NULL,
	[CompanyName] [nvarchar](40) NOT NULL,
	[Phone] [nvarchar](24) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)
ELSE
	TRUNCATE TABLE UK_Shippers


