IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'GLOBAL_Countries')
BEGIN
CREATE TABLE [dbo].[GLOBAL_Countries](
	[CountryKey] [int] IDENTITY(1,1) NOT NULL,
	[CountryID] [int] NOT NULL,
	[countryCode] [char](3) NOT NULL,
	[countryName] [nvarchar](60) NOT NULL,
	[countryIndicative] [nvarchar](24) NOT NULL,
	[currencyCode] [char](3) NOT NULL,
	[currencyName] [nvarchar](20) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
 CONSTRAINT [PK_GLOBAL_Countries] PRIMARY KEY CLUSTERED 
(
	[CountryKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [NonClusteredIndex-CountryKey] ON [dbo].[GLOBAL_Countries]
(
	[CountryKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END



