IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Titles')
CREATE TABLE [dbo].[DQP_UK_Titles](
	[TitleID] [int]  NULL,
	[Title] [nvarchar](30) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)


