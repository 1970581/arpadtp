IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Categories')
CREATE TABLE [dbo].[DQP_UK_Categories](
	[CategoryID] [int]   NULL,
	[CategoryName] [nvarchar](15)  NULL,
	[Description] [ntext] NULL,
	[Picture] [image] NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



