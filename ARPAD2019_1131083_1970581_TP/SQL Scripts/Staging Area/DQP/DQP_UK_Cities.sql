IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Cities')
CREATE TABLE [dbo].[DQP_UK_Cities](
	[CityID] [int]  NULL,
	[CityName] [nvarchar](20) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



