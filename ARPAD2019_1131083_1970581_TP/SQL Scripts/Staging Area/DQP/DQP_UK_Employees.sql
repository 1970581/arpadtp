IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Employees')
CREATE TABLE [dbo].[DQP_UK_Employees](
	[EmployeeID] [int]  NULL,
	[LastName] [nvarchar](20)  NULL,
	[FirstName] [nvarchar](10)  NULL,
	[TitleID] [int] NULL,
	[TitleOfCourtesy] [nvarchar](25) NULL,
	[BirthDate] [date] NULL,
	[HireDate] [date] NULL,
	[Address] [nvarchar](60) NULL,
	[CityID] [int] NULL,
	[RegionID] [int] NULL,
	[PostalCode] [nvarchar](10) NULL,
	[HomePhone] [nvarchar](24) NULL,
	[Extension] [nvarchar](4) NULL,
	[Photo] [image] NULL,
	[Notes] [ntext] NULL,
	[ReportsTo] [int] NULL,
	[PhotoPath] [nvarchar](255) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



