IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_DM_OrderDetails')
CREATE TABLE [dbo].[DQP_DM_OrderDetails](
	[OrderID] [int]   NULL,
	[OrderID_UK] [int] NULL,
	[OrderID_US] [int] NULL,
	[Delegation] [nvarchar](4) NULL,
	[ProductID_KEY] [int]  NULL,
	[ProductID] [int]  NULL,
	[UnitPrice] [money]  NULL,
	[CurrencyType] [nchar](3) NULL,
	[Quantity] [smallint]  NULL,
	[Discount] [float]  NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[SupplierID] [int] NULL,
	[DQP] [nvarchar](256) NULL
)



