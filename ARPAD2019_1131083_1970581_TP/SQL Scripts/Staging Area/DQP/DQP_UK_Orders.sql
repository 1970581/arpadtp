IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Orders')
CREATE TABLE [dbo].[DQP_UK_Orders](
	[OrderID] [int] NULL,
	[CustomerID] [int] NULL,
	[EmployeeID] [int] NULL,
	[OrderDate] [date] NULL,
	[RequiredDate] [date] NULL,
	[ShippedDate] [date] NULL,
	[ShipperID] [int] NULL,
	[Freight] [money] NULL,
	[ShipToID] [int] NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



