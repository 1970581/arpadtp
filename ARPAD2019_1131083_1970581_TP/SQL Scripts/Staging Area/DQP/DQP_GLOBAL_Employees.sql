IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_GLOBAL_Employees')
CREATE TABLE [dbo].[DQP_GLOBAL_Employees](
	[EmployeeID] [int]  NULL,
	[EmployeeID_UK] [int] NULL,
	[EmployeeID_US] [int] NULL,
	[Delegation] [nvarchar](4) NULL,
	[LastName] [nvarchar](20)  NULL,
	[FirstName] [nvarchar](10)  NULL,
	[TitleID_UK] [int] NULL,
	[Title] [nvarchar](35) NULL,
	[FullName] [nvarchar](55) NULL,
	[TitleOfCourtesy] [nchar](4) NULL,
	[Gender] [nchar](10) NULL,
	[BirthDate] [date] NULL,
	[HireDate] [date] NULL,
	[Address] [nvarchar](60) NULL,
	[CityID_UK] [int] NULL,
	[City] [nvarchar](20)  NULL,
	[RegionID] [int] NULL,
	[Region] [nchar](50)  NULL,
	[PostalCode] [nvarchar](10) NULL,
	[HomePhone] [nvarchar](24) NULL,
	[Extension] [nvarchar](4) NULL,
	[Photo] [image] NULL,
	[Notes] [ntext] NULL,
	[ReportsTo] [int] NULL,
	[PhotoPath] [nvarchar](255) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL
)


