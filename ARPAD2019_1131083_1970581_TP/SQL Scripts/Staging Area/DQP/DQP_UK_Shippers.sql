IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Shippers')
CREATE TABLE [dbo].[DQP_UK_Shippers](
	[ShipperID] [int]   NULL,
	[CompanyName] [nvarchar](40)  NULL,
	[Phone] [nvarchar](24) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



