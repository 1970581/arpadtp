IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_OrderDetails')
CREATE TABLE [dbo].[DQP_UK_OrderDetails](
	[OrderID] [int]  NULL,
	[ProductID] [int]  NULL,
	[UnitPrice] [money]  NULL,
	[Quantity] [smallint]  NULL,
	[Discount] [float]  NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



