IF NOT EXISTS (SELECT name FROM sys.tables where name = 'DQP_DM_FactCurrency')
BEGIN
CREATE TABLE [dbo].[DQP_DM_FactCurrency](
	[SourceCurrencyKey] [int]  NULL,
	[DestinationCurrencyKey] [int]  NULL,
	[DateKey] [int]  NULL,
	[Date] [date] NULL,
	[SourceCurrency] [nchar](3) NULL,
	[DestinationCurrency] [nchar](3) NULL,
	[ExchangeRate] [float]  NULL,
	[DQP] [nvarchar](256) NULL
	)
END


