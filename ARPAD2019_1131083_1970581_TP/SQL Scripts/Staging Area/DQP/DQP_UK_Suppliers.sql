IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Suppliers')
CREATE TABLE [dbo].[DQP_UK_Suppliers](
	[SupplierID] [int] NULL,
	[CompanyName] [nvarchar](40) NULL,
	[ContactName] [nvarchar](30) NULL,
	[ContactTitleID] [int] NULL,
	[Address] [nvarchar](60) NULL,
	[CityID] [int] NULL,
	[RegionID] [int] NULL,
	[PostalCode] [nvarchar](10) NULL,
	[CountryID] [int] NULL,
	[Phone] [nvarchar](24) NULL,
	[Fax] [nvarchar](24) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)

