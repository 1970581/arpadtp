IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Countries')
CREATE TABLE [dbo].[DQP_UK_Countries](
	[CountryID] [int]  NULL,
	[CountryName] [nvarchar](20) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



