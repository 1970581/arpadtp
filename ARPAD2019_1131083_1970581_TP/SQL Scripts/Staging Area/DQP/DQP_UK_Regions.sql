IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_Regions')
CREATE TABLE [dbo].[DQP_UK_Regions](
	[RegionID] [int]  NULL,
	[RegionDescription] [nchar](50)  NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



