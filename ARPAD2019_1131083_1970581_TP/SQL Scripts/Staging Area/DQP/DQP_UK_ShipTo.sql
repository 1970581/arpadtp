IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'DQP_UK_ShipTo')
CREATE TABLE [dbo].[DQP_UK_ShipTo](
	[ShipToID] [int]   NULL,
	[ShipToName] [nvarchar](30) NULL,
	[ShipToAddress] [nvarchar](30) NULL,
	[ShipCityID] [int] NULL,
	[ShipRegionID] [int] NULL,
	[ShipPostalCode] [nvarchar](10) NULL,
	[ShipCountryID] [int] NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[DQP] [nvarchar](256) NULL,
	)



