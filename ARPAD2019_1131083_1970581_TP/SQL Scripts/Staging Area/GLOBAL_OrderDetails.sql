IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'GLOBAL_OrderDetails')
CREATE TABLE [dbo].[GLOBAL_OrderDetails](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID_UK] [int] NULL,
	[OrderID_US] [int] NULL,
	[Delegation] [nvarchar](4) NULL,
	[ProductID_KEY] [int]  NULL,
	[ProductID] [int]  NULL,
	[UnitPrice] [money]  NULL,
	[CurrencyType] [nchar](3) NULL,
	[Quantity] [smallint]  NULL,
	[Discount] [float]  NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	[SupplierID] [int] NULL
 CONSTRAINT [PK_GLOBAL_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



