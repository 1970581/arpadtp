IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'lookupDelegation')
BEGIN
	CREATE TABLE [dbo].[lookupDelegation](
		[Delegation] [nvarchar](4) NOT NULL,
		[DelegationName] [nvarchar](20) NULL,
		[countryCode] [char](3) NOT NULL,
		[countryName] [nvarchar](20) NOT NULL,
		[currency] [char](3) NOT NULL
	)
	INSERT [dbo].[lookupDelegation] ([Delegation], [DelegationName], [countryCode], [countryName], [currency]) VALUES (N'US', N'United States', N'USA', N'United States', N'USD')
	INSERT [dbo].[lookupDelegation] ([Delegation], [DelegationName], [countryCode], [countryName], [currency]) VALUES (N'UK', N'United Kingdom', N'GBR', N'United Kingdom', N'GBP')
END
