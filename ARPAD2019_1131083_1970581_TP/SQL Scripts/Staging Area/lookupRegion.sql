IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'lookupRegion')
BEGIN
	CREATE TABLE [dbo].[lookupRegion](
		[regionID] [int] NOT NULL,	
		[regionDescription] [nchar](50) null
	)
	INSERT [dbo].[lookupRegion] ([regionID], [regionDescription]) VALUES (1, N'Eastern')
	INSERT [dbo].[lookupRegion] ([regionID], [regionDescription]) VALUES (2, N'Western')
	INSERT [dbo].[lookupRegion] ([regionID], [regionDescription]) VALUES (3, N'Northern')
	INSERT [dbo].[lookupRegion] ([regionID], [regionDescription]) VALUES (4, N'Southern')
END
