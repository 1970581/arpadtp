IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'UK_OrderDetails')
CREATE TABLE [dbo].[UK_OrderDetails](
	[OrderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[Discount] [float] NOT NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)
ELSE
	TRUNCATE TABLE UK_OrderDetails


