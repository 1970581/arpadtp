IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'GLOBAL_Customers')
BEGIN
CREATE TABLE [dbo].[GLOBAL_Customers](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID_UK] [int]  NULL,
	[CustomerID_US] [char](10)  NULL,
	[CompanyName] [nvarchar](40) NULL,
	[ContactName] [nvarchar](30) NULL,
	[ContactTitleID_UK] [int] NULL,
	[ContactTitle] [nvarchar](30) NULL,
	[Address] [nvarchar](60) NULL,
	[CityID_UK] [int] NULL,
	[City] [nvarchar](20) NULL,
	[RegionID] [int] NULL,
	[Region] [nvarchar](50) NULL,
	[PostalCode] [nvarchar](10) NULL,
	[CountryID] [int] NULL,
	[CountryCode] [char](3) NULL,
	[Country] [nvarchar](20) NULL,
	[Phone] [nvarchar](24) NULL,
	[Fax] [nvarchar](24) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
CONSTRAINT [PK_GLOBAL_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-CostumerID] ON [dbo].[GLOBAL_Customers]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END


