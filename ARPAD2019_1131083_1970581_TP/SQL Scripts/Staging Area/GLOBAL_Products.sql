IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'GLOBAL_Products')
begin
CREATE TABLE [dbo].[GLOBAL_Products](
	[ProductID_KEY] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[Delegation] [nvarchar](4) NULL,
	[ProductName] [nvarchar](40) NULL,
	[SupplierID] [int] NULL,
	[CategoryID] [int] NULL,
	[CategoryName] [nvarchar](15) NULL,
	[Description] [ntext] NULL,
	[QuantityPerUnitID] [int] NULL,
	[QuantityPerUnit] [nchar](20) NULL,
	[UnitPrice] [money] NULL,
	[UnitPriceUS] [money] NULL,
	[CurrencyType] [nchar](3) NULL,
	[UnitsInStock] [smallint] NULL,
	[UnitsOnOrder] [smallint] NULL,
	[ReorderLevel] [smallint] NULL,
	[Discontinued] [char](5) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
 CONSTRAINT [PK_GLOBAL_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID_KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-ProductKey] ON [dbo].[GLOBAL_Products]
(
	[ProductID_KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

end


