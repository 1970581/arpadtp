IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'GLOBAL_Orders')
Begin
CREATE TABLE [dbo].[GLOBAL_Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[OrderID_UK] [int]  NULL,
	[OrderID_US] [int]  NULL,
	[CustomerID] [int] NULL,
	[EmployeeID] [int] NULL,
	[OrderDate] [date] NULL,
	[RequiredDate] [date] NULL,
	[ShippedDate] [date] NULL,
	[ShipperID] [int] NULL,
	[Freight] [money] NULL,
	[CurrencyType] [nchar](3) null,
	[ShipToID] [int] NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL
	 CONSTRAINT [PK_GLOBAL_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [NonClusteredIndex-OrderID] ON [dbo].[GLOBAL_Orders]
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
End


