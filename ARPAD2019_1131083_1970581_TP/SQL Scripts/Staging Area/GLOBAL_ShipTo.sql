IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'GLOBAL_ShipTo')
Begin
CREATE TABLE [dbo].[GLOBAL_ShipTo](
	[ShipToID] [int] IDENTITY(1,1) NOT NULL,
	[ShipToID_UK] [int]  NULL,
	[ShipToID_US] [int]  NULL,
	[Delegation] [nvarchar](4) null,
--	[ShipToName] [nvarchar](30) NULL,
	[ShipToName] [nvarchar](60) NULL,
--	[ShipToAddress] [nvarchar](30) NULL,
	[ShipToAddress] [nvarchar](60) NULL,
	[ShipCityID] [int] NULL,
	[ShipCity] [nvarchar](20) NULL,
	[ShipRegionID_UK] [int] NULL,
	[ShipRegionDescription] [nvarchar](50) NULL,
	[ShipPostalCode] [nvarchar](10) NULL,
	[CountryCode] [char](3) NULL,
	[ShipCountryID] [int] NULL,
	[ShipCountry] [nvarchar](60) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
 CONSTRAINT [PK_GLOBAL_ShipTo] PRIMARY KEY CLUSTERED 
(
	[ShipToID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [NonClusteredIndex-ShipToKey] ON [dbo].[GLOBAL_ShipTo]
(
	[ShipToID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
End

