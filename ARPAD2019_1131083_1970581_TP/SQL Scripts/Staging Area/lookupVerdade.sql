IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'lookupVerdade')
BEGIN
	CREATE TABLE [dbo].[lookupVerdade](
		[Bit] [bit] NOT NULL,
		[Valor] [char](5) NOT NULL,
		[Desc] [char](3) NOT NULL
	)
	INSERT [dbo].[lookupVerdade] ([Bit], [Valor], [Desc]) VALUES (0, N'False', N'nao')
	INSERT [dbo].[lookupVerdade] ([Bit], [Valor], [Desc]) VALUES (1, N'True ', N'sim')
END

