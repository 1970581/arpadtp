IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'UK_Regions')
CREATE TABLE [dbo].[UK_Regions](
	[RegionID] [int] NOT NULL,
	[RegionDescription] [nchar](50) NOT NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)
ELSE
	TRUNCATE TABLE UK_Regions


