IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'UK_Countries')
CREATE TABLE [dbo].[UK_Countries](
	[CountryID] [int] NOT NULL,
	[CountryName] [nvarchar](20) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)
ELSE
	TRUNCATE TABLE UK_Countries


