IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'UK_QuantitiesPerUnit')
CREATE TABLE [dbo].[UK_QuantitiesPerUnit](
	[QuantityPerUnitID] [int]  NOT NULL,
	[QuantityPerUnit] [nchar](20) NULL,
	)
ELSE
	TRUNCATE TABLE UK_QuantitiesPerUnit


