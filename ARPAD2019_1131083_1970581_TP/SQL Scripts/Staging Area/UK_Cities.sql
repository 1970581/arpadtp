IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'UK_Cities')
CREATE TABLE [dbo].[UK_Cities](
	[CityID] [int] NOT NULL,
	[CityName] [nvarchar](20) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
	)
ELSE
	TRUNCATE TABLE UK_Cities


