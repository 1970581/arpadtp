IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'GLOBAL_Regions')
Begin
CREATE TABLE [dbo].[GLOBAL_Regions](
	[RegionKey] [int] IDENTITY(1,1) NOT NULL,
	[RegionID_UK] [int] NULL,
	[RegionID_US] [int] NULL,
	[RegionDescription] [nchar](50) NULL,
	[CreateDate] [date] NULL,
	[LastUpdate] [date] NULL,
 CONSTRAINT [PK_GLOBAL_Regions] PRIMARY KEY CLUSTERED 
(
	[RegionKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [NonClusteredIndex-RegionKey] ON [dbo].[GLOBAL_Regions]
(
	[RegionKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
End
