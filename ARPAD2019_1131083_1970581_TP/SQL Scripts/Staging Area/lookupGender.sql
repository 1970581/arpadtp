IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = 'lookupGender')
BEGIN
	CREATE TABLE [dbo].[lookupGender](
		[TitleOfCourtesy] [nchar](4) NOT NULL,
		[gender] [char](1) NOT NULL,
		[genderName] [nvarchar](10) NOT NULL
	)	
	INSERT [dbo].[lookupGender] ([TitleOfCourtesy], [gender], [genderName]) VALUES (N'Mr.', N'm', N'male')
	INSERT [dbo].[lookupGender] ([TitleOfCourtesy], [gender], [genderName]) VALUES (N'Mrs.', N'f', N'female')
	INSERT [dbo].[lookupGender] ([TitleOfCourtesy], [gender], [genderName]) VALUES (N'Ms.', N'f', N'female')
	INSERT [dbo].[lookupGender] ([TitleOfCourtesy], [gender], [genderName]) VALUES (N'Dr.', N'u', N'unknown')
END
